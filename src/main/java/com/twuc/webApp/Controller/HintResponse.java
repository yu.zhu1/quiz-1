package com.twuc.webApp.Controller;

public class HintResponse {
    HintResponse() {
    }

    public String getHint() {
        return hint;
    }

    public boolean isCorrect() {
        return correct;
    }

    private String hint;
    private boolean correct;


    void calcResult(String guess, String answer) {
        char[] guessNumbers = guess.toCharArray();
        char[] answerNumbers = answer.toCharArray();

        int A = 0;
        int B = 0;

        for (int i = 0; i < guessNumbers.length; i++) {
            if (guessNumbers[i] == answerNumbers[i]) {
                A++;
                continue;
            }
            for (char answerNumber : answerNumbers) {
                if (guessNumbers[i] == answerNumber) {
                    B++;
                }
            }
        }

        this.hint = String.format("%dA%dB", A, B);
        this.correct = this.hint.equals("4A0B");
        System.out.println("hint:" + this.hint);
        System.out.println(this.correct);
    }
}
