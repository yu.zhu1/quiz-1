package com.twuc.webApp.Controller;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.lang.reflect.Array;
import java.util.*;
import java.util.stream.Collector;

@RestController
public class CreateGameController {

    private int id;

    public HashMap<Integer, Game> gameCache = new HashMap<>();

    @PostMapping("/api/games")
    ResponseEntity createGame() {
        Game game = new Game(++id);
        int id = game.getId();
        gameCache.put(id, game);
        return ResponseEntity.status(201).header("Location", String.format("/api/games/%d",id)).build();
    }

    @GetMapping("/api/games/{gameId}")
    ResponseEntity getAnswer(@PathVariable int gameId) {
        Game game = gameCache.get(gameId);
        if (game == null) {
            return ResponseEntity.notFound().build();
        }
        return ResponseEntity.status(200).body(game);
    }



    @PatchMapping("/api/games/{gameId}")
    ResponseEntity getHint(@PathVariable int gameId, @RequestBody GuessNumber guessNumber) {
        Game game = gameCache.get(gameId);
        if (game == null) {
            return ResponseEntity.notFound().build();
        }
        String guess = guessNumber.getAnswer();

        String answer = game.getAnswer();

        if (guess.toCharArray().length != 4 || !guess.matches("^[0-9]{4}$")) {
            return ResponseEntity.status(400).build();
        }


        long countDistinct = guess.chars().distinct().count();
        if (countDistinct <4) {
            return ResponseEntity.status(400).build();
        }

        HintResponse hintResponse = new HintResponse();
        hintResponse.calcResult(guess,answer);
        return ResponseEntity.status(200).body(hintResponse);
    }










}
