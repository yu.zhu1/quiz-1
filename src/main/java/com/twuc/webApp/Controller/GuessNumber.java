package com.twuc.webApp.Controller;

public class GuessNumber {
    public GuessNumber() {
    }

    public String getAnswer() {
        return answer;
    }

    public GuessNumber(String answer) {
        this.answer = answer;
    }

    private String answer;

}
