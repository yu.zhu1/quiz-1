package com.twuc.webApp.Entity;

public class Game {

    private int gameId;

    public String getAnswer() {
        return answer;
    }

    private String answer;

    public Game() {
    }

    public Game(int gameId, String answer) {
        this.gameId = gameId;
        this.answer = answer;
    }

    public int getGameId() {
        return gameId;
    }


}

