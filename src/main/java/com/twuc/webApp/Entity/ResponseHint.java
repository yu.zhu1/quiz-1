package com.twuc.webApp.Entity;

public class ResponseHint {
    private boolean correct;
    private String hint;

    public ResponseHint(String hint, boolean correct) {
        this.hint = hint;
        this.correct = correct;
    }

    public String getHint() {
        return hint;
    }

    public boolean isCorrect() {
        return correct;
    }


}
