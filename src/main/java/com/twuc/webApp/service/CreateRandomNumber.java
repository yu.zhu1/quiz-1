package com.twuc.webApp.service;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;

public class CreateRandomNumber {

    private static final Random gen = new Random();


    public String printRandomFourDigitsNumber() {


        int[] result = new int[4];
        Set<Integer> used = new HashSet<Integer>();

        for (int i = 0; i < 10; i++) {

            int newRandom;
            do {
                newRandom = gen.nextInt(10);
            } while (used.contains(newRandom));
            result[i] = newRandom;
            used.add(newRandom);
        }

        String concat = Integer.toString(result[0]) + Integer.toString(result[1]) +  Integer.toString(result[2]) +  Integer.toString(result[3]);

        return concat;
    }


}
