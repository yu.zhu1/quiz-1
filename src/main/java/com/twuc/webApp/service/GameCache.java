package com.twuc.webApp.service;

import com.twuc.webApp.Entity.Game;
import com.twuc.webApp.Entity.ResponseHint;

import java.util.HashMap;
import java.util.Random;

public class GameCache {
    private HashMap<Integer, Game> gameCache = new HashMap<Integer, Game>();

   public Game createGame(){
       CreateRandomNumber createRandomNumber = new CreateRandomNumber();
       String answer = createRandomNumber.printRandomFourDigitsNumber();
       Random random = new Random();
       int gameId = random.nextInt(1000);
       Game theGame = new Game(gameId, answer);
       gameCache.put(gameId, theGame);
       return theGame;
   }

   public Game getGameById(int gameId) {
       return gameCache.get(gameId);
   }

   public ResponseHint checkAnswer(int gameId, String answer) {
       return new ResponseHint("4A0B", true);
   }


}
