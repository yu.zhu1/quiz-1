package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.Controller.HintResponse;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class GuessNumberTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_get_hint_when_guess_the_number() throws Exception {
        mockMvc.perform(post("/api/games")).andExpect(MockMvcResultMatchers.status().is(201));
        String feedBack = mockMvc.perform(patch("/api/games/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{ \"answer\": \"1234\" }"))
                .andExpect(status().is(200))
                .andExpect(content().contentType("application/json;charset=UTF-8")).andReturn().getResponse().getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();
        HintResponse hintResponse = objectMapper.readValue(feedBack, HintResponse.class);
        assertTrue(hintResponse.getHint().matches("^[0-4]A[0-4]B$"));
        assertEquals(hintResponse.isCorrect(), hintResponse.getHint().equals("4A0B"));
    }

    @Test
    void should_get_the_correct_to_be_true_if_answer_is_correct() throws Exception {
        mockMvc.perform(post("/api/games"));
        String feedBack = mockMvc.perform(patch("/api/games/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{ \"answer\": \"1234\" }"))
                .andExpect(status().is(200))
                .andExpect(content().contentType("application/json;charset=UTF-8")).andReturn().getResponse().getContentAsString();

        ObjectMapper objectMapper = new ObjectMapper();
        HintResponse hintResponse = objectMapper.readValue(feedBack, HintResponse.class);
        assertTrue(hintResponse.isCorrect());
        assertEquals(hintResponse.isCorrect(), hintResponse.getHint().equals("4A0B"));
    }

    @Test
    void should_return_status_404_if_no_gameId_exists() throws Exception {
        mockMvc.perform(post("/api/games"));
        mockMvc.perform(patch("/api/games/999")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{ \"answer\": \"1234\" }"))
                .andExpect(status().is(404));
    }

    @Test
    void should_return_status_400_when_gameId_is_of_wrong_style() throws Exception {
        mockMvc.perform(patch("/api/games/abc")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{ \"answer\": \"1234\" }"))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_400_when_answer_is_of_wrong_style() throws Exception {
        mockMvc.perform(post("/api/games"));
        mockMvc.perform(patch("/api/games/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{ \"answer\": \"12345\" }"))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_400_when_answer_contains_letter() throws Exception {
        mockMvc.perform(post("/api/games"));
        mockMvc.perform(patch("/api/games/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{ \"answer\": \"123a\" }"))
                .andExpect(status().is(400));
    }

    @Test
    void should_return_400_when_answer_contains_duplicated_number() throws Exception {
        mockMvc.perform(post("/api/games"));
        mockMvc.perform(patch("/api/games/1")
                .contentType(MediaType.APPLICATION_JSON_VALUE)
                .content("{ \"answer\": \"1233\" }"))
                .andExpect(status().is(400));
    }
}
