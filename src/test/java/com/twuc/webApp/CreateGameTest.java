package com.twuc.webApp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.Controller.Game;
import org.hamcrest.text.MatchesPattern;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.junit.jupiter.api.Assertions.assertNotEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CreateGameTest {

    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_create_new_game_and_return_201_when_POST_api() throws Exception {
        mockMvc.perform(post("/api/games")).andExpect(MockMvcResultMatchers.status().is(201));
    }

    @Test
    void should_return_201_and_header_Location_when_POST_api() throws Exception {
        mockMvc.perform(post("/api/games"))
                .andExpect(MockMvcResultMatchers.status().is(201));
    }

    @Test
    void should_return_id_and_answer_of_the_game_when_GET_api_with_game_id() throws Exception {
        String location = createGameAndGetUri();
        String gameId = getGameId(location);

        mockMvc.perform(get(location))
                .andExpect(MockMvcResultMatchers.status().is(200))
                .andExpect(MockMvcResultMatchers.content().contentType("application/json;charset=UTF-8"))
                .andExpect(jsonPath("$.id").value(gameId))
                .andExpect(jsonPath("$.answer").value(MatchesPattern.matchesPattern("^[0-9]{4}$")));
    }


    @Test
    void should_create_different_games_with_different_game_ids() throws Exception {
        String location = createGameAndGetUri();
        String gameId = getGameId(location);
        String anotherLocation = createGameAndGetUri();
        String anotherGameId = getGameId(anotherLocation);
        assertNotEquals(gameId, anotherGameId);
    }

    @Test
    void should_create_game_with_answer_of_no_duplicate_numbers() throws Exception {
        String location = createGameAndGetUri();

        String contentAsString = mockMvc.perform(get(location)).andReturn().getResponse().getContentAsString();
        ObjectMapper objectMapper = new ObjectMapper();
        Game game = objectMapper.readValue(contentAsString, Game.class);
        boolean duplicates = false;
    }

    private String createGameAndGetUri() throws Exception {
        return mockMvc.perform(post("/api/games")).andReturn().getResponse().getHeader("Location");
    }

    private String getGameId(String location) {
        int lastSlash = location.lastIndexOf('/');
        return location.substring(lastSlash+1);
    }

    @Test
    void should_return_404_when_gameId_not_exists() throws Exception {
        final String notExistedGamId = "9999";
        mockMvc.perform(get("api/games/"+notExistedGamId)).andExpect(status().is(404));
    }
}
